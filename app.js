var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var stylus = require('stylus');

var index = require('./routes/index');
var users = require('./routes/users');

//var http = require('http');
//
//http.createServer(function (req, res) {
//    res.writeHead(200, {'Content-Type': 'text/plain'});
//    res.end('Hello World!');
//}).listen(8080);
//


var app = express();

var firebase = require("firebase");

// Initialize Firebase
// TODO: Replace with your project's customized code snippet
var config = {
    apiKey: "AIzaSyCGshuTLU7vJdZpe0mc8evuBEGjp_xKNDU",
    authDomain: "fire-chat-b15b1.firebaseapp.com",
    databaseURL: "https://fire-chat-b15b1.firebaseio.com",
    projectId: "fire-chat-b15b1",
    storageBucket: "fire-chat-b15b1.appspot.com",
    messagingSenderId: "569263639060"
  };

firebase.initializeApp(config);



var ref = firebase.database().ref('node-client');
var messageRef = ref.child('messages');

messageRef.push({
    name: 'Chris',
    admin: true,
    count: 1,
    text: 'Hey!'
})

messageRef.push({
    name: 'ММММ',
    admin: true,
    count: 1,
    text: 'Hello!'
})

//var admin = require("firebase-admin");
//var uid = "some-uid";
//admin.auth().createCustomToken(uid)
//  .then(function(customToken) {
//    // Send token back to client
//    console.log("Token: "+customToken);
//  })
//  .catch(function(error) {
//    console.log("Error creating custom token:", error);
//  });




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
